<?php
/**
 * Created by PhpStorm.
 * User: valkeru
 * Date: 16.10.18
 * Time: 20:30
 */

namespace App\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class UserStatusType extends AbstractEnumType
{
    protected $name = 'user_status_type';

    public const STATUS_BLOCKED = 'blocked';
    public const STATUS_ACTIVE  = 'active';

    protected static $choices = [
        self::STATUS_BLOCKED => 'blocked',
        self::STATUS_ACTIVE  => 'active'
    ];
}
