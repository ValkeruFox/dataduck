<?php

namespace App\Form;

use App\Request\RegistrationRequest;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->setMethod(Request::METHOD_POST)
            ->add('login', TextType::class, ['label' => 'Имя пользователя'])
            ->add('email', EmailType::class, ['label' => 'E-mail'])
            ->add('password', PasswordType::class, ['label' => 'Пароль'])
            ->add('password_confirmation', PasswordType::class, [
                'label'         => 'Подтверждение пароля',
                'property_path' => 'passwordConfirmation',
            ])
            ->add('submit', SubmitType::class, ['label' => 'Зарегистрироваться']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => RegistrationRequest::class,
        ]);
    }
}
