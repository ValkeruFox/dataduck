<?php
/**
 * Created by PhpStorm.
 * User: valkeru
 * Date: 16.10.18
 * Time: 23:32
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{
    /**
     * @Route("/", name="main_page")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function actionIndex()
    {
        return $this->render('index/index.html.twig');
    }
}
