<?php

namespace App\Controller;

use App\Form\LoginFormType;
use App\Service\AuthenticationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;

class LoginController extends AbstractController
{
    private $authenticationService;

    public function __construct(AuthenticationService $authenticationService)
    {
        $this->authenticationService = $authenticationService;
    }

    /**
     * @Route("/login", name="login", methods={"GET"})
     * @param Request $request
     *
     * @return Response
     */
    public function actionIndex(Request $request): Response
    {
        $session = $request->getSession();

        if (!$session || !$session->has('userName')) {
            $form = $this->createForm(LoginFormType::class);

            return $this->render('login/index.html.twig', [
                'form' => $form->createView(),
            ]);
        }

        return $this->render('login/success.html.twig', [
            'username' => $session->get('userName')
        ]);
    }

    /**
     * @Route("/login", methods={"POST"})
     * @param Request $request
     *
     * @return Response
     */
    public function actionLogin(Request $request): Response
    {
        $form = $this->createForm(LoginFormType::class);
        $form->handleRequest($request);

        try {
            $isValidData = $this->authenticationService->authenticateUser($form->getData());
        } catch (AccessDeniedHttpException $exception) {
            $form->addError(new FormError($exception->getMessage()));
            return $this->render('login/index.html.twig', [
                'form' => $form->createView()
            ]);
        }

        if (!$isValidData) {
            $form->addError(new FormError('Учётная запись не существует или не активирована'));
            return $this->render('login/index.html.twig', [
                'form' => $form->createView()
            ]);
        }

        $session = $request->getSession();
        $session->set('userName', $form->getData()->getLogin());

        return $this->render('login/success.html.twig', [
            'username' => $session->get('userName')
        ]);
    }
}
