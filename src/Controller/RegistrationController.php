<?php

namespace App\Controller;

use App\Form\RegistrationFormType;
use App\Service\RegistrationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RegistrationController extends AbstractController
{
    private $registrationService;

    private $mailer;

    private $sender;

    public function __construct(RegistrationService $registrationService, \Swift_Mailer $mailer, ContainerInterface $container)
    {
        $this->registrationService = $registrationService;
        $this->mailer = $mailer;
        $this->sender = $container->getParameter('email_sender');
    }

    /**
     * @Route("/registration", name="registration", methods={"GET"})
     */
    public function index()
    {
        $form = $this->createForm(RegistrationFormType::class);

        return $this->render('registration/index.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/registration", methods={"POST"})
     * @param Request $request
     *
     * @return Response
     */
    public function actionRegister(Request $request): Response
    {
        $form = $this->createForm(RegistrationFormType::class);
        $form->handleRequest($request);

        if (!$form->isSubmitted()) {
            return $this->redirectToRoute('registration');
        }

        if (!$form->isValid()) {
            return $this->render('registration/index.html.twig', [
                'form' => $form->createView()
            ]);
        }

        $user = $this->registrationService->registerUser($form->getData());

        $subject = 'Регистрация учётной записи';
        $message = (new \Swift_Message($subject))
            ->setFrom($this->sender)
            ->setTo($user->getEmail())
            ->setBody($this->renderView('registration/email.html.twig', [
                'title' => $subject,
                'email' => $user->getEmail(),
                'id'    => $user->getId(),
                'code'  => $user->getActivationCode()
            ]), 'text/html');

        $this->mailer->send($message);

        return $this->render('registration/complete.html.twig', [
            'email' => $user->getEmail()
        ]);
    }

    /**
     * @Route("/activate", name="activation", methods={"GET"})
     * @param Request $request
     *
     * @return Response
     */
    public function actionActivate(Request $request): Response
    {
        $session = $request->getSession();
        $session->clear();

        $data = $request->query->all();
        $failed = false;

        if (!\array_key_exists('id', $data)) {
            $this->addFlash('notice', 'Не задан ID пользователя');
            $failed = true;
        }

        if (!\array_key_exists('code', $data)) {
            $this->addFlash('notice', 'Не задан код активации');
            $failed = true;
        }

        if ($failed) {
            return $this->render('registration/activation_failed.html.twig');
        }

        $result = $this->registrationService->activateUser($data['id'], $data['code']);

        if (!$result) {
            return $this->render('registration/activation_failed.html.twig');
        }

        $this->addFlash('notice', 'Учётная запись успешно активирована');
        return $this->redirectToRoute('login');
    }
}
