<?php
/**
 * Created by PhpStorm.
 * User: valkeru
 * Date: 16.10.18
 * Time: 23:00
 */

namespace App\Service;

use App\Entity\User;
use App\Request\LoginRequest;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Facebook\WebDriver\Cookie;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AuthenticationService
{
    private $entityManager;
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function authenticateUser(LoginRequest $request): bool
    {
        try {
            $user = $this->entityManager->getRepository(User::class)->findByLogin($request->getLogin());
        } catch (NoResultException | NonUniqueResultException $resultException) {
            return false;
        }

        if (!$user->checkPassword($request->getPassword())) {
            throw new AccessDeniedHttpException('Неверный пароль');
        }

        return true;
    }
}
