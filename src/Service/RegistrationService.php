<?php
/**
 * Created by PhpStorm.
 * User: valkeru
 * Date: 16.10.18
 * Time: 20:25
 */

namespace App\Service;

use App\DBAL\Types\UserStatusType;
use App\Entity\User;
use App\Request\RegistrationRequest;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

/**
 * Class RegistrationService
 *
 * @package App\Service
 */
class RegistrationService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * RegistrationService constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param RegistrationRequest $registrationRequest
     *
     * @return User
     */
    public function registerUser(RegistrationRequest $registrationRequest): User
    {
        $user = new User();
        $user->setLogin($registrationRequest->getLogin())
            ->setPassword($registrationRequest->getPassword())
            ->setEmail($registrationRequest->getEmail());

        $this->entityManager->persist($user);
        $this->entityManager->flush();
        $this->entityManager->refresh($user);

        return $user;
    }

    /**
     * @param int    $userId
     * @param string $activationCode
     *
     * @return bool
     */
    public function activateUser(int $userId, string $activationCode): bool
    {
        try {
            $user = $this->entityManager->getRepository(User::class)->findByIdAndActivationCode($userId, $activationCode);
        } catch (NoResultException | NonUniqueResultException $noResultException) {
            return false;
        }

        $user->setStatus(UserStatusType::STATUS_ACTIVE);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return true;
    }
}
