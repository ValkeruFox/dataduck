<?php
/**
 * Created by PhpStorm.
 * User: valkeru
 * Date: 16.10.18
 * Time: 20:44
 */

namespace App\Repository;

use App\DBAL\Types\UserStatusType;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class UserRepository extends ServiceEntityRepository
{
    /**
     * @var \Doctrine\ORM\QueryBuilder
     */
    private $qb;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
        $this->qb = $this->createQueryBuilder('u');
    }

    /**
     * @param int    $id
     * @param string $activationCode
     *
     * @return User
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findByIdAndActivationCode(int $id, string $activationCode): User
    {
        return $this->qb->andWhere('u.id = :id')->andWhere('u.activationCode = :activationCode')
            ->andWhere('u.status = :status')
            ->setParameters([
                'id'             => $id,
                'activationCode' => $activationCode,
                'status'         => UserStatusType::STATUS_BLOCKED,
            ])
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * @param string $login
     *
     * @return User
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findByLogin(string $login): User
    {
        return $this->qb->andWhere('u.login = :login')->andWhere('u.status = :status')
            ->setParameters([
                'login' => $login,
                'status' => UserStatusType::STATUS_ACTIVE
            ])
            ->getQuery()
            ->getSingleResult();
    }
}
