<?php
/**
 * Created by PhpStorm.
 * User: valkeru
 * Date: 16.10.18
 * Time: 20:12
 */

namespace App\Request;

use Symfony\Component\Validator\Constraints as Assert;

class RegistrationRequest
{
    /**
     * @var string | null
     * @Assert\NotBlank()
     */
    private $login;

    /**
     * @var string | null
     * @Assert\Length(min="8", minMessage="Минимальная длина пароля - {{ limit }} символов")
     * @Assert\NotBlank()
     */
    private $password;

    /**
     * @var string | null
     * @Assert\NotBlank()
     * @Assert\Expression("value === this.getPassword()", message="Пароль и подтверждение не совпадают")
     */
    private $passwordConfirmation;

    /**
     * @var string | null
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @return null|string
     */
    public function getLogin(): ?string
    {
        return $this->login;
    }

    /**
     * @param null|string $login
     */
    public function setLogin(string $login): void
    {
        $this->login = $login;
    }

    /**
     * @return null|string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param null|string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return null|string
     */
    public function getPasswordConfirmation(): ?string
    {
        return $this->passwordConfirmation;
    }

    /**
     * @param null|string $passwordConfirmation
     */
    public function setPasswordConfirmation(string $passwordConfirmation): void
    {
        $this->passwordConfirmation = $passwordConfirmation;
    }

    /**
     * @return null|string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param null|string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }
}
